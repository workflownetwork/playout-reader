//
//  MTPlaylistStatus.swift
//  Playout Reader
//
//  Created by Matt Timmons on 12/22/17.
//  Copyright © 2017 Production Workflow Solutions. All rights reserved.
//

import Cocoa

public let kMTPlaylistRefreshTime = 2.0

protocol MTPlaylistStatusDelegate {
    func playlistHasUpdate(_ readerObj: MTPlaylistStatus, error: Error?)
    func readerIsWaitng(_ readerObj: MTPlaylistStatus)
}

class MTPlaylistStatus: NSObject {
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
    
    var delegate: MTPlaylistStatusDelegate?
    private var refreshTimer: Timer?
    var connectAddress: String?
    var jsonInfoString = "No Status"
    var playListItems: [OnTheAirPlaylistItem]?
    public var playlistIdx:Int = 0
    
    private var apiTask: URLSessionDataTask?
    var waitCount: Int = 0
    
    public func startWatchingPlaylistWithAddress(addressString:String, delegate: MTPlaylistStatusDelegate) {
        
        self.delegate = delegate
        self.connectAddress = "http://" + addressString + ":8081"
        let query = "/playlists/" + "\(playlistIdx)" + "/" + "items"
        self.connectAddress?.append(query)
        startRefreshTimer()
        
    }
    
    public func stopWatching() {
        
        DispatchQueue.main.async {
            
            self.delegate = nil
            self.refreshTimer?.invalidate()
            self.refreshTimer = nil
            self.apiTask?.cancel()
        }
    }
    
    private func loadPlayoutInfo() {
        
        guard let endpoint = self.connectAddress else {
            DispatchQueue.main.async {
                let err = BackendError.urlError(reason: "No Address")
                self.delegate?.playlistHasUpdate(self, error: err)
            }
            return
        }
        
        
        let queue = DispatchQueue(label: "loadPlayoutStatusQueue")
        queue.async {
            
            self.playoutAPI(endpoint, completionHandler: { (err) in
                
                if err != nil {
                    print(err.debugDescription)
                    self.jsonInfoString = err.debugDescription
                }
                
                DispatchQueue.main.async {
                    self.delegate?.playlistHasUpdate(self, error: err)
                }
            })
        }
    }
    
    private func startRefreshTimer() {
        
        refreshTimer = Timer.scheduledTimer(withTimeInterval: kMTPlaylistRefreshTime, repeats: true, block: { (timer) in
            self.loadPlayoutInfo()
        })
        refreshTimer?.fire()
    }
    
    private func playoutAPI(_ endpoint: String, completionHandler: @escaping (Error?) -> Void) {
        
        guard let url = URL(string: endpoint) else {
            print("Error: cannot create URL")
            let error = BackendError.urlError(reason: "Could not construct URL")
            completionHandler(error)
            return
        }
        
        // Make request
        //let urlRequest = URLRequest(url: url)
        
        let session = URLSession.shared
        if apiTask?.state == .running {
            delegate?.readerIsWaitng(self)
            waitCount += 1
            print("!!! Task running.. wait... \(waitCount)")
            if waitCount > 60 {
                let err = BackendError.urlError(reason: "Time to update is failing")
                completionHandler(err)
            }
            return
        }
        
        apiTask = session.dataTask(with: url) { (data, response, error) in
            
            self.waitCount = 0
            guard error == nil else {
                completionHandler(error!)
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                let error = BackendError.objectSerialization(reason: "No data in response")
                completionHandler(error)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                if let jsonString = String(data: responseData, encoding: .utf8) {
                    self.jsonInfoString = jsonString
                    //print(jsonString)
                }

                self.playListItems = try decoder.decode([OnTheAirPlaylistItem].self, from: responseData)
                completionHandler(nil)
                
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                completionHandler(error)
                return
            }
            
        }
        apiTask?.resume()
    }
    
}

//
//  PlayOutClasses.swift
//  Playout Reader
//
//  Created by Matt Timmons on 8/2/17.
//  Copyright © 2017 Production Workflow Solutions. All rights reserved.
//

import Foundation

struct OnTheAirStatus: Codable {
    
    let itemDuration: Double?
    let isPlaying: Bool
    let isPaused: Bool
    
    let playlistElapsed: Double
    let playlistDuration: Double
    let itemElapsed: Double?
    let itemIndex: Int?
    let itemRemaining: Double?
    let playlistRemaining: Double
    let itemDisplayName: String?
    let itemPath: String?
    let uniqueID: String?
    
    enum CodingKeys: String, CodingKey {
        
        case itemDuration = "item_duration"
        case isPlaying = "is_playing"
        case isPaused = "is_paused"
        case playlistElapsed = "playlist_elapsed"
        case playlistDuration = "playlist_duration"
        case itemElapsed = "item_elapsed"
        case itemIndex = "item_index"
        case itemRemaining = "item_remaining"
        case playlistRemaining = "playlist_remaining"
        case itemDisplayName = "item_display_name"
        case itemPath = "item_path"
        case uniqueID = "item_unique_id"
    }
    /*
     {
     "item_duration": 226.7600734067401,
     "is_playing": true,
     "item_unique_id": "FF879F46-BB4B-4EC5-BED3-5D6C18C36440",
     "playlist_elapsed": 82.11544878211545,
     "playlist_duration": 226.7600734067401,
     "item_elapsed": 82.11544878211545,
     "item_index": 1,
     "item_remaining": 124.6246246246246,
     "playlist_remaining": 124.6246246246246
     }
     */
}

struct OnTheAirPlaylistItem: Codable {
    
    let duration: Double
    let name: String
    private let playbackMode: Double
    let itemPath: String
    let uniqueID: String
    var clipEndMode: EndPlaybackMode  {
        return EndPlaybackMode.init(rawValue: playbackMode) ?? .none
    }
    
    enum EndPlaybackMode: Double {
        case none = 0
        case loop = 4
        case autoPlay = 3
    }
    
    enum CodingKeys: String, CodingKey {
        
        case playbackMode = "playback_mode"
        case itemPath = "file_path"
        case duration = "duration"
        case name = "name"
        case uniqueID = "unique_id"
    }
    
    /*
     validation_status: 2,
     reverse_field_order: false,
     live: false,
     logo_enabled: false,
     duration: 66.19947052001953,
     video_codec: "Apple ProRes 422 (LT)",
     has_in_point: false,
     unique_id: "F36E636F-E016-4178-9F0E-5CD8C7C25DBB",
     closed_captions: [
     "-"
     ],
     logo_filename: "",
     name: "008_RedManGroup_kEm_6T1h_V3",
     drop_frame: false,
     audio_codec: "PCM",
     frames_per_second: 29.96999931335449,
     subtitles: [ ],
     validation_error: 0,
     playback_mode: 3,
     file_path: "/_Deliveries/Elements/Specific_Deliveries/Grapevine_Specific/2017-12-20_ChristmasShow/008_RedManGroup_kEm_6T1h_V3.mov",
     audio_volume: 1,
     out_point: 0,
     actions: [ ],
     in_point: 0,
     has_out_point: false,
     languages: [ ]
     */
    
}

struct OnTheAirPlaylistInfo: Codable {
    
    let path: String
    let name: String
    let uniqueID: String
    let playerDescription: String
    let isFront: Bool
    
    enum CodingKeys: String, CodingKey {
        
        case isFront = "is_front"
        case uniqueID = "unique_id"
        case playerDescription = "player_description"
        case name
        case path
    }
    
    /*
     {
     path: "/Users/softron/Documents/C3 2018/C3 2018 - Session 1.xpls",
     is_front: true,
     unique_id: "FE17011A-443A-4845-9347-717D143E15E9",
     name: "C3 2018 - Session 1",
     player_description: "UltraStudio 4K (1080i59.94)"
     }
     */
    
}




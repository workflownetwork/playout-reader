//
//  MTTimeUtilities.swift
//
//  Created by Matt Timmons on 7/15/16.


import Foundation

struct MTTimeUtilities {
    
    
    static func timeConvertToString(_ secondsToConvert: Double) -> String {

        let displayHours = Int(secondsToConvert / 60 / 60)
        let displayMinutes = Int(secondsToConvert / 60.0) - (displayHours * Int(60))
        let displaySeconds = Int(secondsToConvert) - (Int(secondsToConvert)/60) * 60
        
        var timeDisplayString = String(format: "%02u:%02u", displayMinutes, displaySeconds)
        if displayHours != 0 {
            timeDisplayString =  String(format: "%d:%02u:%02u", displayHours, displayMinutes, displaySeconds)
        }
        let milliString = String(format: "%.1f", secondsToConvert)
        let trimmed = milliString.substring(from: milliString.characters.index(milliString.endIndex, offsetBy: -1))
        
        timeDisplayString =  String(format: "%d:%02u:%02u.%@", displayHours, displayMinutes, displaySeconds, trimmed)
        return timeDisplayString
    }
    
    static func timeConvertToStringhhmmss(_ secondsToConvert: Double) -> String {
        
        let displayHours = Int(secondsToConvert / 60 / 60)
        let displayMinutes = Int(secondsToConvert / 60.0) - (displayHours * Int(60))
        let displaySeconds = Int(secondsToConvert) - (Int(secondsToConvert)/60) * 60
        
        var timeDisplayString = String(format: "%02um %02us", displayMinutes, displaySeconds)
       // if displayHours != 0 {
            //timeDisplayString =  String(format: "%dh %02um %02us", displayHours, displayMinutes, displaySeconds)
            timeDisplayString =  String(format: "%d:%02u:%02u", displayHours, displayMinutes, displaySeconds)

       // }
        
        return timeDisplayString
    }
    
    static func timeConvertMilliToString(_ secondsToConvert: Double) -> String {
        
        let displayHours = Int(secondsToConvert / 60 / 60)
        let displayMinutes = Int(secondsToConvert / 60.0) - (displayHours * Int(60))
        let displaySeconds = Int(secondsToConvert) - (Int(secondsToConvert)/60) * 60
        
        var timeDisplayString = String(format: "%02u:%02u", displayMinutes, displaySeconds)
        if displayHours != 0 {
            timeDisplayString =  String(format: "%d:%02u:%02u", displayHours, displayMinutes, displaySeconds)
        }
        
        let milliString = String(format: "%.1f", secondsToConvert)
        let trimmed = milliString.substring(from: milliString.characters.index(milliString.endIndex, offsetBy: -1))
        
        timeDisplayString = timeDisplayString + "." + trimmed
        
        return timeDisplayString
    }
    
    static func secondsFromTimeString(_ timeString: String) -> Double {
        
        let timeComponents = timeString.components(separatedBy: ":")
        
        var hours: Int = 0
        var mins: Int = 0
        var secs: Int = 0
        
        if timeComponents.count == 1 {
            secs = Int(timeComponents[0]) ?? 0
        }
        else if timeComponents.count == 2 {
            mins = Int(timeComponents[0]) ?? 0
            secs = Int(timeComponents[1]) ?? 0
        }
        else if timeComponents.count == 3 {
            hours = Int(timeComponents[0]) ?? 0
            mins = Int(timeComponents[1]) ?? 0
            secs = Int(timeComponents[2]) ?? 0
        } else {
            secs = Int(timeString)!
        }
        
        let intTime = secs + (mins*60) + (hours * 60 * 60)

        return Double(intTime)
    }
	
	static func timeConvertToStringFormatted(_ secondsToConvert: Double) -> String {
		
		var days = Int(secondsToConvert/(60*60*24))
		var months = Int(days/30)
		let years = Int(months/12)
		
		var hours = Int(secondsToConvert / 60 / 60)
		let minutes = Int(secondsToConvert / 60.0) - (hours * Int(60))
		let seconds = Int(secondsToConvert) - (Int(secondsToConvert)/60) * 60
		
		
		if hours >= 24 {
			hours = hours - (days*24)
		}
		
		if days >= 30 {
			days = days - (months*24)
		}
		
		if months >= 12 {
			months = months - (years*360)
		}
		
		// Text Setup
		
		var sYears = "year"
		var sMonths = "month"
		var sDays = "day"
		var sHours = "hour"
		var sMinutes = "min"
		var sSeconds = "sec"
		
		if years > 1 {
			sYears = "years"
		}
		if months > 1 {
			sMonths = "months"
		}
		if days > 1 {
			sDays = "days"
		}
		if hours > 1 {
			sHours = "hours"
		}
		if minutes > 1 {
			sMinutes = "mins"
		}
		if seconds > 1 {
			sSeconds = "secs"
		}
		
		// String Output.
		
		var displayString = ""
		
		
		if seconds > 0 {
			displayString = "\(seconds) \(sSeconds)"
		}
		if minutes > 0 {
			displayString = "\(minutes) \(sMinutes)  \(seconds) \(sSeconds)"
		}
		if minutes > 30 {
			displayString = "\(minutes) \(sMinutes)"
		}
		if hours > 0 {
			displayString = "\(hours) \(sHours)  \(minutes) \(sMinutes)"
		}
		if hours > 4 {
			displayString = "\(hours) \(sHours) "
		}
		if days > 0 {
			displayString = "\(days) \(sDays)  \(hours) \(sHours)"
		}
		if days > 7 {
			displayString = "\(days) \(sDays)"
		}
		if months > 0 {
			displayString = "\(months) \(sMonths)  \(days) \(sDays)"
		}
		if months > 3 {
			displayString = "\(months) \(sMonths) "
		}
		if years > 0 {
			displayString = "\(years) \(sYears)  \(months) \(sMonths)"
		}
		if years > 2 {
			displayString = "\(years) \(sYears)"
		}
		
		
		return displayString
	}
}

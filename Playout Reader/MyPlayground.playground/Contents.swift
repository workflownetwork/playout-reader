//: A Cocoa based Playground to present user interface

import AppKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

struct OnTheAirStatus: Codable {
    
    let itemDuration: Double
    let isPlaying: Bool
    let itemUniqueID: String
    let playlistElapsed: Double
    let playlistDuration: Double
    let itemElapsed: Double
    let itemIndex: Int
    let itemRemaining: Double
    let playlistRemaining: Double
    
    enum CodingKeys: String, CodingKey {
        
        case itemDuration = "item_duration"
        case isPlaying = "is_playing"
        case itemUniqueID = "item_unique_id"
        case playlistElapsed = "playlist_elapsed"
        case playlistDuration = "playlist_duration"
        case itemElapsed = "item_elapsed"
        case itemIndex = "item_index"
        case itemRemaining = "item_remaining"
        case playlistRemaining = "playlist_remaining"
    }

    /*
     {
     "item_duration": 226.7600734067401,
     "is_playing": true,
     "item_unique_id": "FF879F46-BB4B-4EC5-BED3-5D6C18C36440",
     "playlist_elapsed": 82.11544878211545,
     "playlist_duration": 226.7600734067401,
     "item_elapsed": 82.11544878211545,
     "item_index": 1,
     "item_remaining": 124.6246246246246,
     "playlist_remaining": 124.6246246246246
     }
     */
}

enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}


let nibFile = NSNib.Name(rawValue:"MyView")
var topLevelObjects : NSArray?

Bundle.main.loadNibNamed(nibFile, owner:nil, topLevelObjects: &topLevelObjects)
let views = (topLevelObjects as! Array<Any>).filter { $0 is NSView }



let topTopView = views[0] as! NSView

let aTextField = NSTextField(frame: topTopView.frame)
topTopView.addSubview(aTextField)
print(aTextField)

aTextField.stringValue = "00:00:00s"
print(aTextField.stringValue)

func fetchPlayoutTime(_ playoutAddress: String, completionHandler: @escaping (String?, Error?) -> Void) {
    
    let endpoint = "http://" + playoutAddress + ":8081" + "/playlists/" + "0/" + "playing"
    print(endpoint)
    //http://10.1.145.121:8081/playlists/0/playing
    
    guard let url = URL(string: endpoint) else {
        print("Error: cannot create URL")
        let error = BackendError.urlError(reason: "Could not construct URL")
        completionHandler(nil, error)
        return
    }
    
    // Make request
    //let urlRequest = URLRequest(url: url)
    
    let session = URLSession.shared
    let task = session.dataTask(with: url) { (data, response, error) in
        
        guard error == nil else {
            completionHandler(nil, error!)
            return
        }
        
        guard let responseData = data else {
            print("Error: did not receive data")
            let error = BackendError.objectSerialization(reason: "No data in response")
            completionHandler(nil, error)
            return
        }
        
        let decoder = JSONDecoder()
        //let playoutStatus = try! decoder.decode(OnTheAirStatus.self, from: responseData)
        //print(playoutStatus.playlist_remaining)
        
        
        
        do {
            
            let jsonString = String(data: responseData, encoding: .utf8)!
            print(jsonString)
            
            let playoutStatus = try decoder.decode(OnTheAirStatus.self, from: responseData)
            print(playoutStatus.isPlaying)
            print(playoutStatus.itemRemaining)
            completionHandler("XXXXXXXXX", nil)
 
     
        } catch {
            // error trying to convert the data to JSON using JSONSerialization.jsonObject
            completionHandler(nil, error)
            return
        }
        
    }
    task.resume()
}

fetchPlayoutTime("10.1.145.121") { (timeString, err) in
    
    if err != nil {
        print(err.debugDescription)
    }
    
    if timeString != nil {
        print(timeString!)
    }
    
    print(aTextField.stringValue)
    
    // Present the view in Playground
    
    
    // Present the view in Playground
    DispatchQueue.main.async {
        PlaygroundPage.current.liveView = views[0] as! NSView
        
        //print("aTextField.stringValue")
        //print(aTextField.stringValue)
    }

}




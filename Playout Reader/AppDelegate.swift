//
//  AppDelegate.swift
//  Playout Reader
//
//  Created by Matt Timmons on 8/1/17.
//  Copyright © 2017 Production Workflow Solutions. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var allWindows = [NSWindowController]()

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    
    @IBAction func newFilePush(_ sender: Any) {
        
       // let wc = NSWindowController()

        let sb = NSStoryboard(name: NSStoryboard.Name(rawValue: "Main"), bundle: nil)
        if let wc = sb.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "ViewerWC")) as? NSWindowController {
     
            
            if let vc =  wc.contentViewController as? PlayoutInfoViewController {
                allWindows.append(wc)
                vc.playlistIdx = allWindows.count
                wc.showWindow(nil)
            }
        }
    }


}

class MTWindow: NSWindow {
    
}


//
//  PlayoutInfoViewController.swift
//  Playout Reader
//
//  Created by Matt Timmons on 8/1/17.
//  Copyright © 2017 Production Workflow Solutions. All rights reserved.
//

import Cocoa

class PlayoutInfoViewController: NSViewController {
    
    @IBOutlet weak var addressEntry: NSTextField!
    @IBOutlet weak var updateButton: NSButton!
    @IBOutlet weak var addressInfoLabel: NSTextField!
    
    @IBOutlet weak var timeElapsedLabel: NSTextField!
    @IBOutlet weak var timeRemainingLabel: NSTextField!
    
    @IBOutlet weak var playlistTableView: NSTableView!
    @IBOutlet weak var playStatusImageView: NSImageView!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    
    @IBOutlet weak var timesView: NSView!
    var addressString: String?
    
    var playoutReader: MTPlayoutReader?
    var playlistStatus: MTPlaylistStatus?
    var playlistInfo: MTPlaylistInfo?
    
    var tableDataArray = [OnTheAirPlaylistItem]()
    public var playlistIdx:Int = 0
    
    override func awakeFromNib() {
        
        self.view.window?.contentView?.wantsLayer = true
        self.view.window?.toolbar?.showsBaselineSeparator = false
        self.view.window?.appearance = NSAppearance.init(named: NSAppearance.Name.vibrantDark)
        self.view.window?.isMovableByWindowBackground = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.wantsLayer = true
        timesView.layer?.borderWidth = 2
        timesView.layer?.cornerRadius = 5
        timesView.layer?.borderColor = NSColor.white.withAlphaComponent(0.50).cgColor
        timesView.layer?.backgroundColor = NSColor.black.withAlphaComponent(0.20).cgColor
        
        playlistTableView.delegate = self
        playlistTableView.dataSource = self
        self.playlistTableView.layout()
        
        resetReader()
       
        
        if let storedAddress = UserDefaults.standard.object(forKey: "connectAddress") as? String {
            addressString = storedAddress
            addressEntry.stringValue = storedAddress
        }

        if addressString == nil {
            updateButton.isEnabled = false
        }
        
        addressEntry.delegate = self
        
        //self.statusInfoView.superview?.superview?.isHidden = true
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    
    @IBAction func updateButtonPush(_ sender: Any) {
        
        if playoutReader != nil {
            resetReader()
            return
        }

        let currentValue = addressEntry.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if currentValue == "" {
            return
        }
        
        UserDefaults.standard.set(currentValue, forKey: "connectAddress")
        UserDefaults.standard.synchronize()
        
        playoutReader = MTPlayoutReader()
        playoutReader?.playlistIdx = self.playlistIdx
        playoutReader?.startWatchingWithAddress(addressString: currentValue, delegate: self)
        
        playlistStatus = MTPlaylistStatus()
        playlistStatus?.playlistIdx = self.playlistIdx
        playlistStatus?.startWatchingPlaylistWithAddress(addressString: currentValue, delegate: self)
        
        playlistInfo = MTPlaylistInfo()
        playlistInfo?.playlistIdx = self.playlistIdx
        playlistInfo?.startWatchingPlaylistWithAddress(addressString: currentValue, delegate: self)
        
        self.updateButton.title = "Disconnect"
        self.addressEntry.isEnabled = false
        self.addressEntry.alphaValue = 0.45
        
    }
    
    
    func resetReader() {
        
        playoutReader?.stopWatching()
        playoutReader = nil
        
        playlistStatus?.stopWatching()
        playlistStatus = nil
        
        self.updateButton.title = "Connect"
        self.addressEntry.isEnabled = true
        self.addressEntry.alphaValue = 1.0
        
        self.addressInfoLabel.stringValue = "Please enter your hostname or IP of the player"
        self.addressInfoLabel.toolTip = ""
        self.timeElapsedLabel.stringValue = "--:--:--"
        self.timeRemainingLabel.stringValue = "--:--:--"
        playStatusImageView.image = nil
        //self.statusInfoView.string = "Not Connected."
        timesView.layer?.borderColor = NSColor.white.withAlphaComponent(0.50).cgColor
        
        self.view.layer?.borderWidth = 0
        self.view.layer?.cornerRadius = 0
        self.view.layer?.borderColor = NSColor.clear.cgColor
        
        if #available(OSX 10.13, *) {
            self.view.window?.tab.title = self.view.window?.title ?? "Playlist"
        } else {
            // Fallback on earlier versions
        }
    }

}


extension PlayoutInfoViewController: NSTextFieldDelegate, NSControlTextEditingDelegate {
    
    override func controlTextDidChange(_ obj: Notification) {
        let currentValue = addressEntry.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if currentValue == "" {
            updateButton.isEnabled = false
        } else {
            updateButton.isEnabled = true
        }
    }
    
    override func controlTextDidEndEditing(_ obj: Notification) {
        let currentValue = addressEntry.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        if currentValue == "" {
            updateButton.isEnabled = false
        } else {
            updateButton.isEnabled = true
        }
    }
}

extension PlayoutInfoViewController: MTPlayoutReaderDelegate {
    
    func readerIsWaitng(_ readerObj: MTPlaylistStatus) {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimation(nil)
        }
    }
    
    func readerHasUpdate(_ readerObj: MTPlayoutReader, error: Error?) {
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimation(nil)
        }
        
        if let errorText = error?.localizedDescription {
            //self.statusInfoView.string = errorText
            print("Error = \(errorText)")
            resetReader()
            return
        }
        
 
        if let name = readerObj.playoutStatus?.itemDisplayName {
            self.addressInfoLabel.stringValue = name.replacingOccurrences(of: "_", with: " ")
            
            if let filePath = readerObj.playoutStatus?.itemPath {
                self.addressInfoLabel.toolTip = filePath
            }
        } else {
            self.addressInfoLabel.stringValue = "Nothing Cued"
        }
        
        /*
        var jsonDisplayString = readerObj.jsonInfoString
        if let address = readerObj.connectAddress {
            jsonDisplayString = address + "\n" + jsonDisplayString
        }
        
        self.statusInfoView.string = jsonDisplayString
        */
        
        if let timeleft = readerObj.playoutStatus?.itemRemaining {
            
            let humanString = MTTimeUtilities.timeConvertToStringhhmmss(timeleft)
            self.timeRemainingLabel.stringValue = humanString
        }
        
        if let timeElapsed = readerObj.playoutStatus?.itemElapsed {
            
            let humanString = MTTimeUtilities.timeConvertToStringhhmmss(timeElapsed)
            self.timeElapsedLabel.stringValue = humanString
        }
        
        if readerObj.playoutStatus?.isPlaying == true {
            
            self.view.layer?.borderWidth = 2
            self.view.layer?.cornerRadius = 2
           
            
            if readerObj.playoutStatus?.isPaused == false {
                timesView.layer?.borderColor = NSColor.green.cgColor
                playStatusImageView.image = #imageLiteral(resourceName: "Play")
                self.view.layer?.borderColor = NSColor.green.cgColor
            } else {
                timesView.layer?.borderColor = NSColor.orange.cgColor
                playStatusImageView.image = #imageLiteral(resourceName: "Pause")
                self.view.layer?.borderColor = NSColor.orange.cgColor
            }
            
            if #available(OSX 10.13, *) {
                self.view.window?.tab.title = "*" + (self.view.window?.title ?? "Playlist") + "*"
            } else {
                // Fallback on earlier versions
            }
            
           
            
        } else {
            timesView.layer?.borderColor = NSColor.blue.withAlphaComponent(0.80).cgColor
            playStatusImageView.image = #imageLiteral(resourceName: "Stop")
            
            self.view.layer?.borderWidth = 0
            self.view.layer?.cornerRadius = 0
            self.view.layer?.borderColor = NSColor.clear.cgColor
            
            if #available(OSX 10.13, *) {
                self.view.window?.tab.title = self.view.window?.title ?? "Playlist"
            } else {
                // Fallback on earlier versions
            }
        }
    }

}

extension PlayoutInfoViewController: MTPlaylistStatusDelegate {
    
    func readerIsWaitng(_ readerObj: MTPlayoutReader) {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimation(nil)
        }
    }
    
    func playlistHasUpdate(_ readerObj: MTPlaylistStatus, error: Error?) {
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimation(nil)
            
            if let errorText = error?.localizedDescription {
                //self.statusInfoView.string = errorText
                print("Error = \(errorText)")
                self.resetReader()
                return
            }
            
            if let playlist = readerObj.playListItems {
                
                self.tableDataArray.removeAll()
                self.tableDataArray = playlist
                self.playlistTableView.reloadData()
                
            }
        }
        
       
    }
}


extension PlayoutInfoViewController: MTPlaylistInfoDelegate {
    func readerIsWaitng(_ readerObj: MTPlaylistInfo) {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimation(nil)
        }
    }
    
    func playlistInfoHasUpdate(_ readerObj: MTPlaylistInfo, error: Error?) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimation(nil)
            self.title = readerObj.playListInfo?.name ?? "Playlist"
            self.view.window?.title = readerObj.playListInfo?.name ?? "Playlist"
        }
    }

}

extension PlayoutInfoViewController: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return tableDataArray.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        let obj = tableDataArray[row]
        
        if (tableColumn?.identifier)!.rawValue == "status" {
            
            let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "statusCell"), owner: nil) as? NSTableCellView
            
            if let currentItem = playoutReader?.playoutStatus {
              
                if currentItem.itemDisplayName != nil {
                    
                    if currentItem.uniqueID?.lowercased() == obj.uniqueID.lowercased() {
                        if currentItem.isPlaying == true {
                            
                            if currentItem.isPaused == false {
                                cell?.imageView?.image = NSImage(named: .statusAvailable)
                            } else {
                                cell?.imageView?.image = NSImage(named: .statusPartiallyAvailable)
                            }
                        } else {
                            cell?.imageView?.image = NSImage(named: .statusNone)
                        }
                    } else {
                        cell?.imageView?.image = NSImage(named: .statusNone)
                    }
                    
                } else {
                   cell?.imageView?.image = NSImage(named: .statusUnavailable)
                }

            } else {
                cell?.imageView?.image = NSImage(named: .statusUnavailable)
            }
            
            return cell
            
        }
        if (tableColumn?.identifier)!.rawValue == "fileName" {
            
            let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "nameCell"), owner: nil) as? NSTableCellView
            cell?.textField?.stringValue = obj.name
            return cell
        }
        if (tableColumn?.identifier)!.rawValue == "playMode" {
            let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "modeCell"), owner: nil) as? NSTableCellView
            
            switch obj.clipEndMode {
            case .none:
                cell?.textField?.stringValue = ""
            case .loop:
                cell?.textField?.stringValue = "⟳"
            case .autoPlay:
                cell?.textField?.stringValue = "⬇︎"
            }
            return cell
        }
        if (tableColumn?.identifier)!.rawValue == "duration" {
            let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "durationCell"), owner: nil) as? NSTableCellView
            
            cell?.textField?.stringValue = MTTimeUtilities.timeConvertToString(obj.duration)
            return cell
        }
        
        
        
        return nil
    }
    
}


